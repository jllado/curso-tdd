package com.iexpertos.encriptor;

public interface Printer {
    public void print(String string);
}
