package juego.cartas;

import java.security.InvalidParameterException;

public class JuegoDeLaCartaMasAlta {

    public static final String RANKING_DE_LAS_CARTAS = "123456789JQK";

    public String juegaPartida(String[] manoJugador1, String[] manoJugador2) {

        int victoriasJugador1 = 0;
        int victoriasJugador2 = 0;

        if (sonMismoNumeroDeCartas(manoJugador1, manoJugador2)) {
            throw new InvalidParameterException();
        }

        //La variable i la nombraría mejor, como ronda...
        for (int i = 0; i < manoJugador1.length; i++) {
            if (!esCartaValida(manoJugador1[i])
                    || !esCartaValida(manoJugador2[i])) {
                throw new InvalidParameterException();
            }
            if (esPrimeraCartaMayor(manoJugador1[i], manoJugador2[i])) {
                victoriasJugador1++;
            } else if (esPrimeraCartaMayor(manoJugador2[i], manoJugador1[i])) {
                victoriasJugador2++;
            }
        }

        return quienGana(victoriasJugador1, victoriasJugador2);
    }

    private boolean sonMismoNumeroDeCartas(String[] manoJugador1,
            String[] manoJugador2) {
        return manoJugador1.length != manoJugador2.length;
    }

    private boolean esPrimeraCartaMayor(String carta1, String carta2) {
        return RANKING_DE_LAS_CARTAS.indexOf(carta1) > RANKING_DE_LAS_CARTAS
                .indexOf(carta2);
    }

    private boolean esCartaValida(String carta1) {
        return RANKING_DE_LAS_CARTAS.indexOf(carta1) != -1;
    }

    private String quienGana(int victoriasJugador1, int victoriasJugador2) {
        if (victoriasJugador1 < victoriasJugador2) {
            return "JUGADOR 2 GANA " + victoriasJugador2 + " A "
                    + victoriasJugador1;
        } else if (victoriasJugador1 > victoriasJugador2) {
            return "JUGADOR 1 GANA " + victoriasJugador1 + " A "
                    + victoriasJugador2;
        }

        return "SIN GANADOR";
    }

}
