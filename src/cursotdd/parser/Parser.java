package cursotdd.parser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Parser {

    private Set<String> excludeWords;
    private String alphabetCharacters = "aeiou";
    private String specialCharacters = "áéíóú";

    public Parser() {
        this.excludeWords = new HashSet<>();
        excludeWords.add("a");
        excludeWords.add("el");
        excludeWords.add("la");
        excludeWords.add("y");
    }

    public List<String> parse(String input) {
        List<String> result = new ArrayList<>();
        String[] words = input.toLowerCase().split(" ");
        for (String word : words) {
            if (isValid(word)) {
                result.add(replaceSpecialCharacters(word));
            }
        }

        return result;
    }

    private boolean isValid(String word) {
        return !excludeWords.contains(word);
    }

    private String replaceSpecialCharacters(String word){
        String result = "";

        for (char character : word.toCharArray()) {
            if (isSpecial(character)) {
                int index = specialCharacters.indexOf(character);
                result += alphabetCharacters.charAt(index);
            }
            else {
                result += character;
            }
        }

        return result;
    }

    private boolean isSpecial(char character) {
        return specialCharacters.indexOf(character) >= 0;
    }
}
