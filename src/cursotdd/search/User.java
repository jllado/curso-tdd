package cursotdd.search;

public class User {

    private String name = "";
    private String profile = "";
    private String place = "";
    public User(String name, String profile, String place) {
        this.name = name;
        this.profile = profile;
        this.place = place;
    }

    public User (String profile) {
        this.profile = profile;
    }
    public String getProfile() {
        return profile;
    }
    public String getName() {
        return name;
    }
    public String getPlace() {
        return place;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        User user = (User) obj;
        return user.getProfile().equals(this.profile);
    }
}
