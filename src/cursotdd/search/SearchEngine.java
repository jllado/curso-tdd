package cursotdd.search;

import java.util.ArrayList;
import java.util.List;

import cursotdd.parser.Parser;

public class SearchEngine {

    private Repository repository;
    private Parser parser;

    public SearchEngine(Repository repository, Parser parser) {
        this.repository = repository;
        this.parser = parser;
    }

    public List<User> findUsers(String profile, String place) {
        List<User> usersByPlace = repository.findBy(place);
        List<User> userByProfileAndPlace = new ArrayList<User>();

        for (User user : usersByPlace) {
            if (parser.parse(profile).contains(user.getProfile())) {
                userByProfileAndPlace.add(user);
            }
        }
        return userByProfileAndPlace;
    }

}
