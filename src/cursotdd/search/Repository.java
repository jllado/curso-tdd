package cursotdd.search;

import java.util.List;

public interface Repository {
    public List<User> findBy(String place);
}
