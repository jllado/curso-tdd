package com.iexpertos.encriptor;

public class PrinterMock implements Printer{

    private String message;

    public void print(String string) {
        message = string;
    }

    public String getMessage() {
        return message;
    }
}
