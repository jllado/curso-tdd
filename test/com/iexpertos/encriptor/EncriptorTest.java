package com.iexpertos.encriptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.InvalidParameterException;

import org.junit.Before;
import org.junit.Test;

public class EncriptorTest {

    private Encriptor encriptor;

    private PrinterMock printer;

    @Before
    public void setup() {
        printer = new PrinterMock();
        encriptor = new Encriptor(printer);
    }

    @Test
    public void cryptWord() {
        assertEquals("Jqnc", encriptor.cryptWord("Hola"));
    }

    @Test(expected = InvalidParameterException.class)
    public void cryptEmptyWord() {
        encriptor.cryptWord(" ");
    }

    @Test
    public void crypWordToNumbers() {
        assertEquals("7411311099", encriptor.cryptWordToNumbers("Hola"));
    }

    @Test
    public void crypEmptyWordToNumbers() {
        try {
            encriptor.cryptWordToNumbers(" ");
        } catch (InvalidParameterException e) {
            assertTrue(true);
        }
    }

    @Test
    public void cryptWordWithReplace() {
        assertEquals("Jola", encriptor.cryptWord("Hola", "H"));
    }

    @SuppressWarnings("deprecation")
    @Test
    public void getWords() {
        assertEquals(new String[]{"Hola", "Mundo"}, encriptor.getWords("Hola Mundo"));
    }

    @Test
    public void printWords() {
        assertEquals("Hola", "Hola");
    }

    @Test
    public void cryptSentence() {
        assertEquals("Jqnc\"Owpfq", encriptor.cryptSentence("Hola Mundo"));
    }

    @Test
    public void generateMessage() {
        assertEquals("<hola><mundo>", encriptor.generateMessage("hola mundo"));
    }

    @Test
    public void imprime_mensaje_correcto() {
        encriptor.printWords("hola mundo");

        assertEquals("<hola><mundo>", printer.getMessage());
    }
}
