package cursotdd.parser;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import cursotdd.parser.Parser;


public class TestParser {

    private Parser parser;

    @Before
    public void setup() {
        parser = new Parser();
    }
    @Test
    public void parserSingleWordAsItIs() {
        List<String> result = parser.parse("camionero");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void parseSingleWordWithUpperCase() {
        List<String> result = parser.parse("CAMIONERO");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void parseAnotherSingleWord() {
        List<String> result = parser.parse("FONTANERO");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("fontanero"));
    }

    @Test
    public void discardsArticlesFromInput() {
        List<String> result = parser.parse("el camionero");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void discardsSeveralArticlesFromInput() {
        List<String> result = parser.parse("a el camionero");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void discardsArticlesFromInputAtTheEnd() {
        List<String> result = parser.parse("camionero el");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void ignoreEspecialCharacters() {
        List<String> result = parser.parse("cámionero");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));
    }

    @Test
    public void ignoreEspecialCharacters2() {
        List<String> result = parser.parse("camíonero");

        assertThat(result.size(), is(1));
        assertThat(result.get(0), is("camionero"));

    }

    @Test
    public void parseSeveralWords() {
        List<String> result = parser.parse("camion camionero");

        assertThat(result.size(), is(2));
        assertThat(result.get(0), is("camion"));
        assertThat(result.get(1), is("camionero"));
    }
}
