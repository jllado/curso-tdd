package cursotdd.search;

import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class HasUserMatcher extends TypeSafeMatcher<List<User>> {

    User expectedUser;
    int expectedNumberOfUsers = 1;

    public HasUserMatcher(User expectedUser){
           this.expectedUser = expectedUser;
    }

    public HasUserMatcher(String expectedProfile) {
        this.expectedUser = new User(expectedProfile);
    }

    @Override
    public void describeTo(Description arg0) {
        // TODO Auto-generated method stub

    }

    public TypeSafeMatcher<List<User>> andThereAreExactly(int expectedNumberOfUsers) {
        this.expectedNumberOfUsers = expectedNumberOfUsers;
        return this;
    }
    @Override
    protected boolean matchesSafely(List<User> users) {
        int counterUser = 0;
        for(User user : users){
            if (user.getProfile().equals(expectedUser.getProfile())) {
                if (expectedNumberOfUsers == 0) {
                    return true;
                }
                counterUser++;
            }
        }

        return counterUser == expectedNumberOfUsers;
    }
}
