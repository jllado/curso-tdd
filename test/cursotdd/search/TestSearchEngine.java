package cursotdd.search;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import cursotdd.parser.Parser;

public class TestSearchEngine {

    private SearchEngine searchEngine;
    private Repository repository;
    private Repository repositoryJMock;
    private Parser parser;
    private List<User> repositoryUsers;
    @Rule public JUnitRuleMockery context = new JUnitRuleMockery();

    @Before
    public void setup() {
        repository = mock(Repository.class);
        repositoryJMock = context.mock(Repository.class);
        parser = new Parser();
        repositoryUsers = new ArrayList<User>();
    }

    @Test
    public void encuentra_el_unico_usuario_que_hay_porque_coincide_con_la_busqueda() {
        repositoryUsers.add(new User("Alberto", "camionero", "Madrid"));
        when(repository.findBy("Madrid")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("camionero", "Madrid");
        assertThat(usuarios.size(), is(1));
        assertThat(usuarios.get(0).getProfile(), is("camionero"));
    }

    @Test
    public void encuentra_el_unico_usuario_que_hay_porque_coincide_con_la_busqueda_con_jmock() {
        repositoryUsers.add(new User("Alberto", "camionero", "Madrid"));

        context.checking(new Expectations(){{
            allowing(repositoryJMock).findBy("Madrid");
            will(returnValue(repositoryUsers));
        }});

        searchEngine = new SearchEngine(repositoryJMock, parser);

        List<User> usuarios = findUser("camionero", "Madrid");
        assertThat(usuarios.size(), is(1));
        assertThat(usuarios.get(0).getProfile(), is("camionero"));
    }

    @Test
    public void aunque_hay_un_usuario_su_perfil_no_coincide_con_la_busqueda() {
        repositoryUsers.add(new User("Alberto", "camionero", "Madrid"));
        when(repository.findBy("Madrid")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("fontanero", "Madrid");
        assertThat(usuarios.size(), is(0));
    }

    @Test
    public void aunque_hay_un_usuario_el_lugar_no_coincide_con_la_busqueda() {
        when(repository.findBy("Valencia")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("camionero", "Valencia");
        assertThat(usuarios.size(), is(0));
    }

    @Test
    public void encuentra_varios_usuarios_para_un_perfil() {
        repositoryUsers.add(new User("Juan", "informatico", "Madrid"));
        repositoryUsers.add(new User("Paco", "informatico", "Madrid"));
        when(repository.findBy("Madrid")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("informatico", "Madrid");
        assertThat(usuarios.size(), is(2));
        assertThat(usuarios.get(0).getProfile(), is("informatico"));
        assertThat(usuarios.get(1).getProfile(), is("informatico"));
    }

    @Test
    public void encuentra_un_usuario_de_un_perfil_al_buscar_por_perfiles_distintos() {
        repositoryUsers.add(new User("Alberto", "camionero", "Madrid"));
        when(repository.findBy("Madrid")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("camionero fontanero", "Madrid");
        assertThat(usuarios.size(), is(1));
        assertThat(usuarios.get(0).getProfile(), is("camionero"));
    }

    @Test
    public void encontramos_usuarios_de_todos_los_perfiles_al_buscar_por_perfiles_disntintos() {
        repositoryUsers.add(new User("Alberto", "camionero", "Madrid"));
        repositoryUsers.add(new User("Juan", "informatico", "Madrid"));
        repositoryUsers.add(new User("Paco", "informatico", "Madrid"));
        when(repository.findBy("Madrid")).thenReturn(repositoryUsers);

        searchEngine = new SearchEngine(repository, parser);

        List<User> usuarios = findUser("camionero informatico", "Madrid");
        assertThat(usuarios.size(), is(3));

        assertThat(usuarios, hasUsersWithProfile("camionero").andThereAreExactly(1));
        assertThat(usuarios, hasUsersWithProfile("informatico").andThereAreExactly(2));
    }

    private HasUserMatcher hasUsersWithProfile(String profile) {
        return new HasUserMatcher(profile);
    }

    private List<User> findUser(String profile, String place) {
        return searchEngine.findUsers(profile, place);
    }

    public class StubRepository implements Repository {
        public List<User> findBy(String place) {
            List<User> usuarios = new ArrayList<User>();
            if ("Madrid".equals(place)) {
                usuarios.add(new User("Alberto", "camionero", "Madrid"));
                usuarios.add(new User("Juan", "informatico", "Madrid"));
                usuarios.add(new User("Paco", "informatico", "Madrid"));
            }
            return usuarios;
        }
    }
}
