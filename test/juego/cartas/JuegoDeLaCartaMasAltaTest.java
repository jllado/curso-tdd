package juego.cartas;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.security.InvalidParameterException;

import org.junit.Before;
import org.junit.Test;

public class JuegoDeLaCartaMasAltaTest {

    private JuegoDeLaCartaMasAlta juegoDeLaCartaMasAlta;

    @Before
    public void setup() {
        juegoDeLaCartaMasAlta = new JuegoDeLaCartaMasAlta();
    }

    @Test
    public void esEmpatePorCartasIgualesConUnaCarta() {
        //Quizás cambiaría el nombre del método juegaPartida, no da la sensación de que devuelva algo, parece que empieza el juego
        //de reaprto de cartas cuando en realidad eso ya ha ocurrido y lo que debe hacer es calcular resultado
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"1"}, new String[]{"1"}), is("SIN GANADOR"));
    }

    @Test
    public void esVictoriaDeJugadorUnoConResultadoUnoACeroConUnaCarta() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"2"}, new String[]{"1"}), is("JUGADOR 1 GANA 1 A 0"));
    }

    @Test
    public void esVictoriaDeJugadorDosConResultadoUnoACeroConUnaCarta() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"1"}, new String[]{"2"}), is("JUGADOR 2 GANA 1 A 0"));
    }

    @Test
    public void esVictoriaDeJugadorUnoConResultadoUnoACeroConConUnaCartaYAmbasCartasFiguras() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"K"}, new String[]{"J"}), is("JUGADOR 1 GANA 1 A 0"));
    }

    @Test
    public void esEmpatePorMismasVictoriasConDosCartas() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"2", "1"}, new String[]{"1", "2"}), is("SIN GANADOR"));
    }

    @Test
    public void esVictoriaDeJugadorDosConResultadoDosACeroConDosCartas() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"1","2"}, new String[]{"J","K"}), is("JUGADOR 2 GANA 2 A 0"));
    }

    @Test
    public void esVictoriaDeJugadorUnoConResultadoDosACeroConDosCartas() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"J","K"}, new String[]{"1","2"}), is("JUGADOR 1 GANA 2 A 0"));
    }

    @Test
    public void esVictoriaDeJugadorUnoConResultadoDosAUnoConTresCartas() {
        assertThat(juegoDeLaCartaMasAlta.juegaPartida(new String[]{"1", "2", "3"}, new String[]{"2", "1", "2"}), is("JUGADOR 1 GANA 2 A 1"));
    }

    @Test(expected=InvalidParameterException.class)
    public void esErrorPorPasarCartasNoExistentes() {
        juegoDeLaCartaMasAlta.juegaPartida(new String[]{"F","G"}, new String[]{"H","Y"});
    }

    @Test(expected=InvalidParameterException.class)
    public void esErrorPorDistintoNumeroDeCartas() {
        juegoDeLaCartaMasAlta.juegaPartida(new String[]{"J","K", "1"}, new String[]{"1","2"});
    }

}
